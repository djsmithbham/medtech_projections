
clear all;

pres='-r600';
pformat='-dpng';
pext='png';

gh=xlsread('GH.xlsx');
rca=xlsread('RCA.xlsx');

GH.lambda=gh(:,1);
GH.raw=gh(:,2:end);
GH.n=size(GH.raw,2);
GH.mean=mean(GH.raw,2);

RCA.lambda=rca(:,1);
RCA.raw=rca(:,2:end);
RCA.n=size(RCA.raw,2);
RCA.mean=mean(RCA.raw,2);

bm1=xlsread('BM1.xlsx');
bm2=xlsread('BM2_GFAP.xlsx');
bm3=xlsread('BM3_S100B.xlsx');

BM1.lambda=bm1(:,1);
BM1.raw=bm1(:,2);
BM2.lambda=bm2(:,1);
BM2.raw=bm2(:,2);
BM3.lambda=bm3(:,1);
BM3.raw=bm3(:,2);

%%
% interpolate data onto equally-spaced wavelength grid between the same limits

sp=1;

lambdaMin=max([GH.lambda(1),  RCA.lambda(1),  BM1.lambda(1),  BM2.lambda(1),  BM3.lambda(1)]);
lambdaMax=min([GH.lambda(end),RCA.lambda(end),BM1.lambda(end),BM2.lambda(end),BM3.lambda(end)]);

lambda.spline=linspace(lambdaMin,lambdaMax,length(GH.lambda));

for j=1:GH.n
    GH.spline(:,j)=csaps(GH.lambda,GH.raw(:,j),sp,lambda.spline)';
end
GH.splinemean=mean(GH.spline,2);
for j=1:RCA.n
    RCA.spline(:,j)=csaps(RCA.lambda,RCA.raw(:,j),sp,lambda.spline)';
end
RCA.splinemean=mean(RCA.spline,2);

BM1.spline=csaps(BM1.lambda,BM1.raw,sp,lambda.spline)';
BM2.spline=csaps(BM2.lambda,BM2.raw,sp,lambda.spline)';
BM3.spline=csaps(BM3.lambda,BM3.raw,sp,lambda.spline)';

for j=1:GH.n
    projections.BM1.GH(j,1)=dot(BM1.spline,GH.spline(:,j))/norm(BM1.spline)^2;
    projections.BM2.GH(j,1)=dot(BM2.spline,GH.spline(:,j))/norm(BM2.spline)^2;
    projections.BM3.GH(j,1)=dot(BM3.spline,GH.spline(:,j))/norm(BM3.spline)^2;
end
for j=1:RCA.n
    projections.BM1.RCA(j,1)=dot(BM1.spline,RCA.spline(:,j))/norm(BM1.spline)^2;
    projections.BM2.RCA(j,1)=dot(BM2.spline,RCA.spline(:,j))/norm(BM2.spline)^2;
    projections.BM3.RCA(j,1)=dot(BM3.spline,RCA.spline(:,j))/norm(BM3.spline)^2;
end

% figure 2c

projections.BM1.RCA_GH=[projections.BM1.RCA;projections.BM1.GH];
projections.BM2.RCA_GH=[projections.BM2.RCA;projections.BM2.GH];
projections.BM3.RCA_GH=[projections.BM3.RCA;projections.BM3.GH];
projections.Groups=[zeros(length(projections.BM1.RCA),1);ones(length(projections.BM1.GH),1)];

figure(2);clf;
plotSpread(projections.BM1.RCA_GH,'distributionIdx',projections.Groups,'distributionMarkers','+');
box on;
set(gca,'xticklabel',{'HV','TBI'});set(gca,'tickdir','out');
%xlabel('Group');
ylabel('Projection on the NAA');

set(2,'paperunits','centimeters');
set(2,'papersize',[8 8]);
set(2,'paperposition',[0 0 8 8]);
print(2,pformat,pres,['figure2c.' pext]);
close(2);

% figure S14
figure(2);clf;
subplot(1,3,1);
plotSpread(projections.BM1.RCA_GH,'distributionIdx',projections.Groups,'distributionMarkers','+');
[projections.BM1.p,projections.BM1.h,projections.BM1.stats]=ranksum(projections.BM1.RCA,projections.BM1.GH);
title(['\(p = ' num2str(projections.BM1.p,'%0.2g') '\)'],'interpreter','latex');
box on;
set(gca,'xticklabel',{'HV','TBI'});set(gca,'tickdir','out');
%xlabel('Group');
ylabel('Projection on the NAA');
%

fid=fopen('figure2_data.txt','w');
fprintf(fid,'Figure 2c data\n\n');
fprintf(fid,'Wilcoxon rank sum, two-tailed\n\n');
fprintf(fid,'n=%i\n\n',length(projections.Groups));
fprintf(fid,'p=%.4e\n\n',projections.BM1.p);
fprintf(fid,'z=%.4e\n\n',projections.BM1.stats.zval);
fclose(fid);

%%

subplot(1,3,2);
plotSpread(projections.BM2.RCA_GH,'distributionIdx',projections.Groups,'distributionMarkers','+');
projections.BM2.p=ranksum(projections.BM2.RCA,projections.BM2.GH);
title(['\(p = ' num2str(projections.BM2.p,'%0.2g') '\)'],'interpreter','latex');
box on;
set(gca,'xticklabel',{'HV','TBI'});set(gca,'tickdir','out');
%xlabel('Group');
ylabel('Projection on the GFAP');
%
subplot(1,3,3);
plotSpread(projections.BM3.RCA_GH,'distributionIdx',projections.Groups,'distributionMarkers','+');
projections.BM3.p=ranksum(projections.BM3.RCA,projections.BM3.GH);
title(['\(p = ' num2str(projections.BM3.p,'%0.2g') '\)'],'interpreter','latex');
box on;
set(gca,'xticklabel',{'HV','TBI'});set(gca,'tickdir','out');
%xlabel('Group');
ylabel('Projection on the S100B');

set(2,'paperunits','centimeters');
set(2,'papersize',[16 8]);
set(2,'paperposition',[0 0 16 8]);
print(2,pformat,pres,['figureS14.' pext]);
close(2);

% supplementary figure 17
% mTBI data is for a smaller range of wavelengths so need to restrict to
% this

clear GH RCA
GH.lambda=gh(:,1);
GH.raw=gh(:,2:end);
GH.n=size(GH.raw,2);
GH.mean=mean(GH.raw,2);

RCA.lambda=rca(:,1);
RCA.raw=rca(:,2:end);
RCA.n=size(RCA.raw,2);
RCA.mean=mean(RCA.raw,2);

mTBI.input0=xlsread('mTBI.xlsx');
mTBI.input1=mTBI.input0;
mTBI.input1(1:2,:)=[];
mTBI.input1=mTBI.input1(end:-1:1,:);% reverse order of rows for this data
mTBI.lambda=mTBI.input1(:,1:3:end); 
mTBI.nlambda=size(mTBI.lambda,2);
mTBI.avlambda=mean(mTBI.lambda,2);
mTBI.devlambda=mTBI.lambda-repmat(mTBI.avlambda,1,mTBI.nlambda);
if length(find(abs(mTBI.devlambda)>1e-6))==0
    'mTBI data has same wavelength values for every patient, passed check'
else
    error('mTBI data has different wavelength values for some patients, failed check')
end

mTBI1.lambda=mTBI.input1(:,1);
mTBI2.lambda=mTBI.input1(:,1);
mTBI.lambda =mTBI.input1(:,1);
n1=33;
ind1=3*(n1-1)+2;
mTBI1.raw=mTBI.input1(:,2:3:ind1);
mTBI2.raw=mTBI.input1(:,ind1+3:3:end);
mTBI.raw =[mTBI1.raw mTBI2.raw];
mTBI1.n=size(mTBI1.raw,2);
mTBI2.n=size(mTBI2.raw,2);
mTBI.n =size(mTBI.raw, 2);

xlswrite('mTBI_cleaned_data.xls',mTBI.raw);
xlswrite('mTBI_cleaned_wavelength.xls',mTBI.lambda);

lambdaMin=max([mTBI.lambda(1),  GH.lambda(1),  RCA.lambda(1),  BM1.lambda(1),  BM2.lambda(1),  BM3.lambda(1)]);
lambdaMax=min([mTBI.lambda(end),GH.lambda(end),RCA.lambda(end),BM1.lambda(end),BM2.lambda(end),BM3.lambda(end)]);

%%
sp=1;
lambda.spline=linspace(lambdaMin,lambdaMax,length(mTBI.lambda));
for j=1:mTBI.n
    mTBI.spline(:,j)=csaps(mTBI.lambda,mTBI.raw(:,j),sp,lambda.spline)';
end
for j=1:GH.n
    GH.spline(:,j)=csaps(GH.lambda,GH.raw(:,j),sp,lambda.spline)';
end
for j=1:RCA.n
    clear RCA.spline
    RCA.spline(:,j)=csaps(RCA.lambda,RCA.raw(:,j),sp,lambda.spline)';
end
BM1.spline=csaps(BM1.lambda,BM1.raw,sp,lambda.spline)';

for j=1:mTBI.n
    projections.BM1.mTBI(j,1)=dot(BM1.spline,mTBI.spline(:,j))/norm(BM1.spline)^2;
end
for j=1:GH.n
    projections.BM1.GH(j,1)=dot(BM1.spline,GH.spline(:,j))/norm(BM1.spline)^2;
end
for j=1:RCA.n
    projections.BM1.RCA(j,1)=dot(BM1.spline,RCA.spline(:,j))/norm(BM1.spline)^2;
end

%%
projections.Groups=[zeros(length(projections.BM1.RCA),1);ones(length(projections.BM1.mTBI),1);2*ones(length(projections.BM1.GH),1)];
projections.BM1.RCA_mTBI_GH.data=[projections.BM1.RCA;projections.BM1.mTBI;projections.BM1.GH];
projections.BM1.RCA_mTBI_GH.p=kruskalwallis(projections.BM1.RCA_mTBI_GH.data,projections.Groups);

figure(2);clf;
pexponent=floor(log10(projections.BM1.RCA_mTBI_GH.p));
pmantissa=projections.BM1.RCA_mTBI_GH.p/10^pexponent;
pvalue=[num2str(pmantissa,2) '\(\times 10^{' num2str(pexponent) '}\)']
plotSpread(projections.BM1.RCA_mTBI_GH.data,'distributionIdx',projections.Groups,'distributionMarkers','+');
title(['\(p\) = ' pvalue],'interpreter','latex');
set(gca,'xticklabel',{'HV','mTBI','sTBI'});box on;set(gca,'tickdir','out');
ylabel('Projection on the NAA');
set(2,'paperunits','centimeters');
set(2,'papersize',[6 8]);
set(2,'paperposition',[0 0 6 8]);
print(2,pformat,pres,['figureS17a.' pext]);
close all;
